# PPK-Praktikum 11 : Android Intent

## Identitas

```
Nama : Novanni Indi Pradana
NIM  : 222011436
Kelas: 3SI3

```

## Deskripsi

Intent adalah sebuah jembatan yang menghubungkan interaksi antar Activity di aplikasi Android. Intent juga dapat membawa dan mengirimkan data ke Activity lainnya, bahkan ke aplikasi lain (Gmail, Google Map dsb). 

Intent merupakan mekanisme untuk melakukan sebuah action dan komunikasi antar komponen aplikasi. Contoh, jika kita memiliki sebuah halaman Activity yang terdapat tombol di dalamnya. Lalu tombol tersebut ditekan untuk membuka peta, kamera, atau halaman lainnya. Mekanisme perpindahan ini dinamakan Intent. 

Intent terbagi menjadi 2 bagian yaitu Intent Implicit dan Intent Explicit. 

Intent Implicit: Berfungsi melakukan perpindahan activity (halaman) menuju ke aplikasi internal. Contohnya ketika hendak membuka sebuah kamera. 

Intent Explicit: Berfungsi melakukan perpindahan activity (halaman) ke activity (halaman) lainnya. Explicit intent bekerja dengan menggunakan nama kelas yang dituju. Umumnya intent ini digunakan untuk mengaktifkan komponen pada satu aplikasi. 



## Kegiatan Praktikum

### Tampilan Menu Awal
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-11/-/raw/main/Screenshot/Screenshot%20(1).png)
### Button 1 Clicked
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-11/-/raw/main/Screenshot/Screenshot%20(2).png)
### Button 2 Clicked
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-11/-/raw/main/Screenshot/Screenshot%20(3).png)


## Penugasan Praktikum

### 1. Button 3 Clicked : Intent Search
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-11/-/raw/main/Screenshot/Screenshot%20(5).png)
### 2. Button 3 Clicked : Intent myIntent
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-11/-/raw/main/Screenshot/Screenshot%20(4).png)
### 3. TwoActivity
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-11/-/raw/main/Screenshot/Screenshot%20(6).png)
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-11/-/raw/main/Screenshot/Screenshot%20(7).png)
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-11/-/raw/main/Screenshot/Screenshot%20(8).png)
